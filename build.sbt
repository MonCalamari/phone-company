lazy val phoneCompany = (project in file(".")).settings(
  Seq(
    name := "disco-test-phone-company",
    version := "1.0",
    scalaVersion := "2.12.3",
    libraryDependencies ++= Seq(
      "org.scalatest"  %% "scalatest"  % "3.0.1" % Test,
      "org.scalacheck" %% "scalacheck" % "1.13.5" % Test
    )
  )
)
