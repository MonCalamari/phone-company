package com.phone.domain

import com.phone.domain.adt.Call
import org.scalatest.{EitherValues, Matchers, WordSpec}

class CallSpec extends WordSpec with Matchers with EitherValues {

  "CallSpec" should {

    "convert string to call" in {
      Call("").isLeft shouldBe true
      Call("A B").isLeft shouldBe true
      Call(" A B C").isLeft shouldBe true
      Call(" A B 00").isLeft shouldBe true
      Call(" A B 00:00").isLeft shouldBe true
      Call(" A B 00:0A").isLeft shouldBe true
      Call(" A B 00:00:00").map(_.duration.valueInSeconds) shouldBe Right(0)
      Call(" A B 00:00:01").map(_.duration.valueInSeconds) shouldBe Right(1)
      Call(" A B 00:01:01").map(_.duration.valueInSeconds) shouldBe Right(61)
      Call(" A B 01:01:01").map(_.duration.valueInSeconds) shouldBe Right(3661)
    }

  }

}
