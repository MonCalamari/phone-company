package com.phone.domain

import com.phone.domain.adt._
import org.scalatest.{EitherValues, Matchers, WordSpec}

class CallServiceSpec extends WordSpec with Matchers with EitherValues {

  "CallService" should {

    "not apply discount for a single call" in {
      val records = parseCalls(
        "A 555-333-212 00:04:28" // (88 * 3) + (180 * 5) = 1164
      )
      val costs = CallService.calculateCost(records)
      costs.size shouldBe 1
      costs.head shouldBe adt.CostPerCustomer(CustomerId("A"), 1164)
    }

    "calculate total cost for a single phone number" in {
      val records = parseCalls(
        "A 555-333-212 00:03:00", // 180 * 5 = 900
        "A 555-333-212 00:01:10", // 70 * 5 = 350
        "A 555-333-212 00:04:28"  // (88 * 3) + (180 * 5) = 1164 (promotion)
      )
      val cost = CallService.calculateCost(records)
      cost.size shouldBe 1
      cost.head shouldBe CostPerCustomer(CustomerId("A"), 1250)
    }

    "calculate total cost for multiple phone numbers" in {
      val records = parseCalls(
        "A 555-333-212 00:02:03", // 123 * 5 = 615
        "A 555-333-212 00:01:10", // 70 * 5 = 350
        "A 555-333-212 00:04:28", // 268 * 5 = 1340 (promotion)
        "A 555-433-242 00:06:41", // (221 * 3) + (180 * 5) = 1563 (promotion)
        "A 555-433-242 00:01:03"  // 63 * 5 = 315
      )

      val costs = CallService.calculateCost(records)
      costs.size shouldBe 1
      costs.head.costInPence shouldBe (315 + 615 + 350)
    }

    "calculate zero cost for zero duration call" in {
      val records = parseCalls("A B 00:00:00")
      val costs = CallService.calculateCost(records)
      costs.headOption shouldBe Some(CostPerCustomer(CustomerId("A"), 0L))
    }

  }

  private def parseCalls(calls: String*): Iterator[Call] = {
    CallParser.parse(StringSource(Iterator(calls: _*))).flatMap(_.right.toOption)
  }

}
