package com.phone.domain

import com.phone.domain.adt.{Call, CallDuration, CustomerId, PhoneNumber}
import org.scalatest.{EitherValues, Matchers, WordSpec}

class CallParserSpec extends WordSpec with Matchers with EitherValues {

  "CallParserSpec" should {

    "parse calls.log string source" in {
      val source = StringSource(
        Iterator(
          "A 555-333-212 00:02:03",
          "A 555-433-242 00:06:41",
          "A 555-433-242 00:01:03",
          "B 555-333-212 00:01:20",
          "A 555-333-212 00:01:10",
          "A 555-663-111 00:02:09",
          "A 555-333-212 00:04:28",
          "B 555-334-789 00:00:03",
          "A 555-663-111 00:02:03",
          "B 555-334-789 00:00:53",
          "B 555-971-219 00:09:51",
          "B 555-333-212 00:02:03",
          "B 555-333-212 00:04:31",
          "B 555-334-789 00:01:59"
        ))
      CallParser.parse(source).foreach(_.isRight shouldBe true)
    }

    "parse calls.log file source" in {
      val source = FileSource.fromClassPath("test.log")
      CallParser.parse(source).foreach(_.isRight shouldBe true)
    }

    "parse logs" in {
      val customerId = "customer-id"
      val phoneNumber = "phone-number"
      val duration = "99:99:99"
      val calls = CallParser.parse(StringSource(Iterator(s"$customerId $phoneNumber $duration"))).toList
      calls.size shouldBe 1
      calls.head shouldBe Right(Call(CustomerId(customerId), PhoneNumber(phoneNumber), CallDuration(362439)))
    }

    "handle empty file" in {
      CallParser.parse(FileSource(() => -1)) should be(empty)
    }
  }

}
