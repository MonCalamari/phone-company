package com.phone.domain

import com.phone.domain.adt.{Call, CostPerCustomer, CostPerPhoneNumber}

private[phone] object CallService {

  private val costPerSecond         = 5
  private val threeMinutesInSeconds = 60 * 3
  private val firstThreeMinutesCost = threeMinutesInSeconds * costPerSecond

  def calculateCost(calls: Iterator[Call]): Iterable[CostPerCustomer] = {
    calls.toSeq.groupBy(_.customerId).map {
      case (customerId, customerCalls) =>
        val costsPerPhoneNumber = customerCalls.groupBy(_.phoneNumber).map {
          case (phoneNumber, phoneCalls) =>
            val costs = phoneCalls.map {
              case call if call.duration.valueInSeconds > threeMinutesInSeconds => (call.duration.valueInSeconds - threeMinutesInSeconds) * 3 + firstThreeMinutesCost
              case call                                                         => call.duration.valueInSeconds * costPerSecond
            }
            val promotion = if (costs.size > 1) costs.max else 0L
            CostPerPhoneNumber(customerId, phoneNumber, costs.sum - promotion)
        }
        CostPerCustomer(customerId, costsPerPhoneNumber.map(_.costInPence).sum)
    }
  }

}
