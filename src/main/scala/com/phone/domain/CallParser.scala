package com.phone.domain

import com.phone.domain.adt.{Call, DomainError}

import scala.io.Source

private[phone] object CallParser {

  def parse(source: CallSource): Iterator[Either[DomainError, Call]] = source match {
    case FileSource(value)   => parse(Source.fromInputStream(value).getLines)
    case StringSource(value) => parse(value)
  }

  private def parse(lines: Iterator[String]): Iterator[Either[DomainError, Call]] = lines.filter(_.nonEmpty).map(Call.apply)

}
