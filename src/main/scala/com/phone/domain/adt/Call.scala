package com.phone.domain.adt

import scala.util.Try

private[phone] final case class CallDuration(valueInSeconds: Long)

private[phone] final case class CustomerId(value: String)

private[phone] final case class PhoneNumber(value: String)

private[phone] final case class Call(customerId: CustomerId, phoneNumber: PhoneNumber, duration: CallDuration)

private[phone] object CallDuration {

  def fromHHMMSS(value: String): Either[DomainError, CallDuration] = value.trim.split(":") match {
    case Array(hh, mm, ss) =>
      for {
        hhInt <- toInt(hh)
        mmInt <- toInt(mm)
        ssInt <- toInt(ss)
        seconds = hhInt * 60 * 60 + mmInt * 60 + ssInt
      } yield CallDuration(seconds)
    case _ => Left(MalformedDuration(value))
  }

  private def toInt(value: String): Either[DomainError, Int] = {
    Try(value.toInt).filter(_ >= 0).toEither.left.map(error => MalformedDuration(error.getMessage))
  }

}

private[phone] object Call {

  implicit val order: Ordering[Call] = Ordering.by(_.duration.valueInSeconds)

  def apply(line: String): Either[DomainError, Call] = line.trim.split(" ") match {
    case Array(customerId, phoneNumber, duration) =>
      CallDuration.fromHHMMSS(duration).map { duration =>
        Call(CustomerId(customerId), PhoneNumber(phoneNumber), duration)
      }
    case _ => Left(MalformedLog(line))
  }

}
