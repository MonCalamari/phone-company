package com.phone.domain.adt

private[phone] final case class CostPerPhoneNumber(customerId: CustomerId, phoneNumber: PhoneNumber, costInPence: Long)

private[phone] final case class CostPerCustomer(customerId: CustomerId, costInPence: Long)
