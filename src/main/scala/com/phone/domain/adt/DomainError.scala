package com.phone.domain.adt

private[phone] trait DomainError

private[phone] case class MalformedLog(line: String) extends DomainError {
  override def toString: String = s"call log is malformed: [$line]"
}

private[phone] case class MalformedDuration(duration: String) extends DomainError {
  override def toString: String = s"call duration is malformed: [$duration]"
}
