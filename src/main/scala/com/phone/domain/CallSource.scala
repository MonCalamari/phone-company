package com.phone.domain

import java.io.InputStream

private[phone] sealed trait CallSource

private[phone] case class FileSource(value: InputStream) extends CallSource

private[phone] case class StringSource(value: Iterator[String]) extends CallSource

private[phone] object FileSource {

  def fromClassPath(file: String): CallSource = FileSource(
    this.getClass.getClassLoader.getResourceAsStream(file)
  )

}
