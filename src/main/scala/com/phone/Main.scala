package com.phone

import com.phone.domain.{CallParser, CallService, FileSource}

object Main {

  private val fileName = "calls.log"

  def main(args: Array[String]): Unit = {
    val source          = FileSource.fromClassPath(fileName)
    val (errors, calls) = CallParser.parse(source).partition(_.isLeft)
    if (errors.nonEmpty) {
      println(s"$fileName contains invalid logs")
      errors.foreach(println)
    }
    val totalCost = CallService.calculateCost(calls.map(_.right.get))
    totalCost.foreach(println)
  }

}
