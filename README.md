# Assumptions

It has been assumed:
- promotion is applied per every phone number group
- promotion applies only to two or more calls (a customer with single very long call wouldn't pay anything)
